/*
 * Copyright (C) 2015  Ovidiu-Florin BOGDAN <ovidiu.b13@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef REPOSSETTINGS_H
#define REPOSSETTINGS_H

#include <QDialog>

namespace Ui
{
class ReposSettings;
}

class ReposSettings : public QDialog
{
    Q_OBJECT

public:
    explicit ReposSettings(QWidget* parent = 0);
    ~ReposSettings();

protected:
    virtual void closeEvent(QCloseEvent* );

private:
    Ui::ReposSettings* ui;
public slots:
    void reject();
    void accept();
    void addRepo();
    void editRepo();
    void deleteRepo();
    void moveRepoUp();
    void moveRepoDown();
};

#endif // REPOSSETTINGS_H
