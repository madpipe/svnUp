/*
 * Copyright (C) 2015  Ovidiu-Florin BOGDAN <ovidiu.b13@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "svnup.h"
#include "ui_svnup.h"
#include <QDir>
#include <QMessageBox>
#include <QTextStream>

svnUp::svnUp(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::svnUp), keepProcessing(true), process(0)
{
    ui->setupUi(this);

    connect(ui->startBtn, SIGNAL(clicked(bool)),
            this, SLOT(startProcessing()));
    connect(ui->cancelBtn, SIGNAL(clicked(bool)),
            this, SLOT(cancelProcessing()));
    connect(ui->actionQuit, SIGNAL(triggered(bool)),
            this, SLOT(close()));
    connect(ui->actionRepositories, SIGNAL(triggered(bool)),
            this,SLOT(editReposSettings()));

    connect(this, SIGNAL(doProcessRepo()),
            this, SLOT(processRepo()));
    connect(this, SIGNAL(doUpdateRepo()),
            this, SLOT(updateRepo()));
    connect(this, SIGNAL(doBuildRepo()),
            this, SLOT(buildRepo()));
    connect(this, SIGNAL(doDoneUpdatingRepo(int)),
            this, SLOT(doneUpdatingRepo(int)));
    connect(this, SIGNAL(doDoneBuildingRepo(int)),
            this, SLOT(doneBuildingRepo(int)));
}

svnUp::~svnUp()
{
    delete ui;
}

void svnUp::addRepo(Repo* repo)
{
    repos.append(repo);
    
    QCheckBox* ckUpdate = new QCheckBox;
//     connect(ckUpdate, SIGNAL(toggled(bool)),
//             row, SLOT(setDoUpdate(bool)));
    QCheckBox* ckBuild = new QCheckBox;
//     connect(ckBuild, SIGNAL(toggled(bool)),
//             row, SLOT(setDoBuild(bool)));

    int row = ui->gridLayout_2->rowCount();
    ui->gridLayout_2->addWidget(new QLabel(repo->name()), row, NAME_COL);
    ui->gridLayout_2->addWidget(ckUpdate, row, UPDATE_COL);
    ui->gridLayout_2->addWidget(ckBuild, row, BUILD_COL);
}

void svnUp::startProcessing()
{
    it = repos.begin();
    keepProcessing = true;
    emit doProcessRepo();
}

void svnUp::processRepo()
{
    if (it != repos.end() && keepProcessing)
    {
        if ((*it)->doUpdate())
        {
            emit doUpdateRepo();
        }
        else if ((*it)->doBuild())
        {
            emit doBuildRepo();
        }
        else
        {
            it++;
            emit doProcessRepo();
        }
    }
}

void svnUp::cancelProcessing()
{
    keepProcessing = false;
  // Process still running
    if ( process && process->state() != QProcess::NotRunning )
    {
        process->terminate();
      // Process not running
        if ( process->state() == QProcess::NotRunning )
            printError("Process Canceled");
    }
}

void svnUp::updateRepo()
{
    printHeader("Updating repo: " + (*it)->name());

    if (!QDir((*it)->localDir()).exists())
    {
        printError("Directory " + (*it)->localDir() + " does not exist.");
        printError("Update failed!");
        emit doDoneUpdatingRepo(-1);
        return;
    }

    process = new QProcess(this);
    process->setWorkingDirectory((*it)->localDir());
    connect(process, SIGNAL(readyReadStandardOutput()),
        this, SLOT(printProcessMessage()));
    connect(process, SIGNAL(readyReadStandardError()),
        this, SLOT(printProcessError()));
    connect(process, SIGNAL(finished(int)),
        this, SLOT(doneUpdatingRepo(int)));

    process->start("svn update");
}

void svnUp::buildRepo()
{
    printHeader("Building repo: " + (*it)->name());
    if (!QDir((*it)->buildDir()).exists())
    {
        printError("Directory " + (*it)->buildDir() + " does not exist.");
        printError("Build failed!");
        emit doDoneBuildingRepo(-1);
        return;
    }
    printMessage((*it)->buildCommands().at(0));
    process = new QProcess(this);
    process->setWorkingDirectory((*it)->buildDir());
    connect(process, SIGNAL(readyReadStandardOutput()),
            this, SLOT(printProcessMessage()));
    connect(process, SIGNAL(readyReadStandardError()),
            this, SLOT(printProcessError()));
    connect(process, SIGNAL(finished(int)),
            this, SLOT(doneBuildingRepo(int)));
    process->start((*it)->buildCommands().at(0));
}

void svnUp::doneUpdatingRepo(int exitCode)
{
    printHeader("Done updating repo: " + (*it)->name());
    if (exitCode != 0)
        printError("Error Code: " + QString::number(exitCode));
    if ((*it)->doBuild())
    {
        emit doBuildRepo();
    }
    else
    {
        it++;
        emit doProcessRepo();
    }
}

void svnUp::doneBuildingRepo(int exitCode)
{
    printHeader("Done building repo: " + (*it)->name());
    if (exitCode != 0)
        printError("Error Code: " + QString::number(exitCode));
    it++;
    emit doProcessRepo();
}

void svnUp::printProcessMessage()
{
    process->setReadChannel(QProcess::StandardOutput);
    QTextStream stream(process);
    while (!stream.atEnd())
        printMessage(stream.readLine());
}

void svnUp::printProcessError()
{
    process->setReadChannel(QProcess::StandardError);
    QTextStream stream(process);
    while (!stream.atEnd())
        printError(stream.readLine());
}

void svnUp::printHeader(QString s)
{
    ui->output->appendHtml("<b>" + s + "</b>");
}

void svnUp::printMessage(QString s)
{
    ui->output->appendHtml(s);
}

void svnUp::printError(QString s)
{
    ui->output->appendHtml("<font color=\"red\">" + s + "</font>");
}

void svnUp::closeEvent(QCloseEvent* bar)
{
    if (process && process->state() != QProcess::NotRunning)
    {
        QMessageBox::StandardButton resBtn;
        QString msg;
        msg = tr("Still running, are you sure you want to quit?");
        resBtn = QMessageBox::question(this, "Confirm quit", msg);
        if (resBtn == QMessageBox::No) {
            bar->ignore();
        } else {
            cancelProcessing();
            bar->accept();
        }
    }
}

void svnUp::editReposSettings()
{
    ReposSettings reposSettings(this);
    reposSettings.exec();
    // TODO update main view
}
