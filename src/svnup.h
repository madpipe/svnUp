/*
 * Copyright (C) 2015  Ovidiu-Florin BOGDAN <ovidiu.b13@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef SVNUP_H
#define SVNUP_H

#include <QMainWindow>
#include <QCheckBox>
#include <QProcess>
#include "repo.h"
#include "repossettings.h"

namespace Ui {
class svnUp;
}

class svnUp : public QMainWindow
{
    Q_OBJECT

#define NAME_COL 0
#define UPDATE_COL 1
#define BUILD_COL 2

public:
    explicit svnUp(QWidget *parent = 0);
    ~svnUp();

    void addRepo(Repo *repo);

private:
    Ui::svnUp *ui;
    QList<Repo*> repos;

    bool keepProcessing;

    QProcess* process;
    QList<Repo*>::iterator it;

    void closeEvent(QCloseEvent *bar);

signals:
    void doProcessRepo();
    void doUpdateRepo();
    void doBuildRepo();
    void doDoneUpdatingRepo(int);
    void doDoneBuildingRepo(int);

private slots:
    void startProcessing();
    void cancelProcessing();

    void updateRepo();
    void buildRepo();
    void processRepo();
    void doneUpdatingRepo(int exitCode);
    void doneBuildingRepo(int exitCode);

    void printHeader(QString);
    void printMessage(QString);
    void printError(QString);

    void printProcessMessage();
    void printProcessError();

    void editReposSettings();
};

#endif // SVNUP_H
