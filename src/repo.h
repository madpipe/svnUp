/*
 * Copyright (C) 2015  Ovidiu-Florin BOGDAN <ovidiu.b13@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#ifndef REPO_H
#define REPO_H
#include <QObject>

class Repo : public QObject
{
    Q_OBJECT
public:
  // Constructors
    Repo(QObject* parent = 0);
    Repo(QString repoName = "", QString repoLocalDir = "", 
         QList<QString> repoBuildCommands = QList<QString>(),
         QString repoBuildDir = "");
  // Operator overloading
    Repo& operator=(const Repo& other);
    bool operator==(const Repo& other) const;
  // Getters
    QString name() { return m_name; };
    QString localDir() { return m_localDir; };
    QString buildDir() { return m_buildDir; };
    QList<QString> buildCommands() { return m_buildCommands; };
    bool doUpdate() { return m_doUpdate; };
    bool doBuild() { return m_doBuild; };
    
private:
  // Members
    QString m_name;
    QString m_localDir;
    QString m_buildDir;
    QList<QString> m_buildCommands;
    bool m_doUpdate;
    bool m_doBuild;
    
public slots:
  //  Setters
    void setName(QString name) { m_name = name; };
    void setLocalDir(QString localDir) { m_localDir = localDir; };
    void setBuildDir(QString buildDir) { m_buildDir = buildDir; };
    void setBuildCommands(QList<QString> buildCommands) { m_buildCommands = buildCommands; };
    void setDoUpdate(bool doUpdate) { m_doUpdate = doUpdate; };
    void setDoBuild(bool doBuild) { m_doBuild = doBuild; };
};

#endif // REPO_H
