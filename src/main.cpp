/*
 * Copyright (C) 2015  Ovidiu-Florin BOGDAN <ovidiu.b13@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "svnup.h"
#include <QApplication>
#include <QDesktopWidget>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    app.setOrganizationName("GA");
    app.setApplicationName("svnUp");

    svnUp w;
    w.show();

    // TODO read this from config file
    QList<QString> l1;
    l1 << "make";
    QList<QString> l2;
    l2 << "sleep 5";
    w.addRepo(new Repo("test1", "/home/bogdano/WORK/test1", l1));
    w.addRepo(new Repo("test2", "/home/bogdano/WORK", l2));

    w.move((QApplication::desktop()->width() - w.width()) / 2,
           (QApplication::desktop()->height() - w.height()) / 2);

    return app.exec();
}
