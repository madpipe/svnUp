/*
 * Copyright (C) 2015  Ovidiu-Florin BOGDAN <ovidiu.b13@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 */

#include "repo.h"

Repo::Repo(QObject* parent): QObject(parent),
    m_name(""), m_localDir(""), m_buildDir(""),
    m_doUpdate(false), m_doBuild(false)
{
}

Repo::Repo(QString repoName, QString repoLocalDir,
           QList< QString > repoBuildCommands, QString repoBuildDir) :
    m_name(repoName), m_localDir(repoLocalDir),
    m_buildCommands(repoBuildCommands), m_buildDir(repoBuildDir),
    m_doUpdate(false), m_doBuild(false)
{
}

Repo& Repo::operator=(const Repo& other)
{
  // if same memory address, skip
    if (this == &other)
      return *this;
    m_name = other.m_name;
    m_localDir = other.m_localDir;
    m_buildDir = other.m_buildDir;
    m_buildCommands = other.m_buildCommands;
    m_doUpdate = other.m_doUpdate;
    m_doBuild = other.m_doBuild;
    return *this;
}

bool Repo::operator==(const Repo& other) const
{
  // same memory address
    if (this == &other)
      return true;
  // same members values
    if ( m_name == other.m_name &&
         m_localDir == other.m_localDir &&
         m_buildDir == other.m_buildDir &&
         m_buildCommands == other.m_buildCommands &&
         m_doUpdate == other.m_doUpdate &&
         m_doBuild == other.m_doBuild )
      return true;
  // different repo
    return false;
}
