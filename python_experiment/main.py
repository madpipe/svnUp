#!/usr/bin/python3
from subprocess import call
from colorama import Fore, Back, Style
from datetime import datetime
import os
import json

# get from config file
work_path = "/home/bogdano/WORK"
projects_db  = os.path.dirname(os.path.realpath(__file__)) + "/projects.json"
log_dir = work_path + "/log"
suported_vcs = ["git", "svn"]

now = datetime.now()
log_dir = "%s/%s-%s-%s" % (log_dir, now.year, now.month, now.day)
log_counter = 0
log_dir_tmp = log_dir
while os.path.exists(log_dir_tmp):
    log_counter += 1
    log_dir_tmp = log_dir + "_" + str(log_counter)
log_dir = log_dir_tmp

svn_revert_command  = ['svn', 'revert',  '--non-interactive', '-R', '.']
svn_update_command  = ['svn', 'update',  '--non-interactive']
svn_cleanup_command = ['svn', 'cleanup', '--non-interactive']

color_cleanup  = Back.YELLOW  + Fore.BLACK
color_revert   = Back.MAGENTA + Fore.WHITE
color_update   = Back.BLUE    + Fore.WHITE
color_build    = Back.GREEN   + Fore.BLACK
color_finished = Back.WHITE   + Fore.BLUE
color_reset    = Style.RESET_ALL
color_error    = Back.RED + Fore.WHITE

class Project:
    def __init__(self, name='', path='', url='', vcs='', dependencies=[],
                 build_command   = [''],
                 run_independent = False):

        self.name         = name # Project name
        self.path         = path # Project path in working directory
        self.url          = url  # Project remote repo URL
        self.vcs          = vcs  # Project version control system
        self.dependencies = dependencies # array of project names

        self.current_wd   = ""

    def create_log_dir(self):
        self.project_log_dir = log_dir + "/" + self.name
        if not os.path.exists(self.project_log_dir):
            os.makedirs(self.project_log_dir)

    def local_project_exists(self):
        if os.path.exists(self.path):
            if self.vcs == "svn":
                print("")
            if self.vcs == "git":
                return_code = call(['git', 'status'])
                if return_code is not 0:
                    return False
        return True

    def pre_command(self, log_file_name, command):
        return_code = 1000
        self.current_wd = os.getcwd()
        if not os.path.exists(self.path):
            print(color_error + "Invalid path: " + self.path + color_reset)
            return return_code
        os.chdir(self.path)
        self.create_log_dir()
        log_file_name = self.project_log_dir + log_file_name
        fobj = open(log_file_name, "w")
        fobj.write('Running command: \n' + ' '.join(command) + '\n in ' + os.getcwd() + '\n\n')
        fobj.flush()
        return fobj

    def post_command(self, fobj, command, return_code):
        fobj.close()
        os.chdir(self.current_wd)
        if return_code != 0:
            print(color_error + "Error: " + str(return_code) + color_reset)
        print(color_finished + "Finished command «" + ' '.join(command) + "»: " + self.name + color_reset)

    def update(self):
        print(color_update + "Updating " + self.name + "..." + color_reset)
        log_file = self.pre_command("/update.log", svn_update_command)
        if log_file == 1:
            return
        return_code = call(svn_update_command, stdout=log_file, stderr=log_file)
        self.post_command(log_file, svn_update_command, return_code)

    def revert(self):
        print(color_revert + "Reverting " + self.name + "..." + color_reset)
        #self.create_log_dir()
        #log_file = os.open(current_logs_dir + "/" + self.name + "/revert.log", "a")
        #return_code = call(svn_revert_command)
        #os.close(log_file)
        #if return_code != 0:
            #print(color_error + "Error cleaning up: " + str(return_code) + color_reset)
        print(color_finished + "Finished reverting " + self.name + color_reset)

    def cleanup(self):
        print(color_cleanup + "Cleaning up " + self.name + "..." + color_reset)
        #self.create_log_dir()
        #log_file = os.open(current_logs_dir + "/" + self.name + "/cleanup.log", "a")
        #return_code = call(svn_cleanup_command)
        #os.close(log_file)
        #if return_code != 0:
            #print(color_error + "Error cleaning up: " + str(return_code) + color_reset)
        print(color_finished + "Finished cleaning up " + self.name + color_reset)

    def build(self):
        print(color_build + "Building " + self.name + "..." + color_reset)
        #self.create_log_dir()
        #log_file = os.open(current_logs_dir + "/" + self.name + "/build.log", "a")
        #return_code = call("make")
        #os.close(log_file)
        #if return_code != 0:
            #print(color_error + "Error cleaning up: " + str(return_code) + color_reset)
        print(color_finished + "Finished building " + self.name + color_reset)

    def execute(self):
        return_code = 1
        current_wd = os.getcwd()
        if not os.path.exists(self.path):
            print(color_error + "Invalid path: " + self.path + color_reset)
            return return_code
        os.chdir(self.path)
        self.create_log_dir()

        self.cleanup()
        self.revert()
        self.update()
        self.build()

        os.chdir(current_wd)
        return 0

def is_project_valid(project):
    # Test Project name
    if not len(project.name) > 0:
        print("Invalid Project name")
        return False
    # Test Project vcs
    vcs_ok = False
    for vcs in suported_vcs:
        if project.vcs == vcs:
            vcs_ok = True
    if not vcs_ok:
        print("Invalid Project vcs")
        return vcs_ok
    return True

def json_to_project(jp):
    project = Project()
    project.name         = jp['name']
    project.path         = work_path + jp['path']
    project.url          = jp['url']
    project.vcs          = jp['vcs']
    project.dependencies = jp['dependencies']
    return project

def get_projects_from_json():
    with open(projects_db) as data_file:
        db = json.load(data_file)
    for jproject in db['projects']:
        project = json_to_project(jproject)
        if is_project_valid(project):
            projects.append(project)
        else:
            print("Project \"" + project.name + "\" is invalid")
            print(jproject)

# get projects form projects json DB
projects = []
get_projects_from_json()

for project in projects:
    project.update()
